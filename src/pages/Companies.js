import React, {useState} from "react";
import {Card, Button, Modal, Form, Table, ListGroup} from 'react-bootstrap'
import {host} from "../util/hosts";


export default function Companies() {
    //const user = JSON.parse(localStorage.getItem('logged-user'));

    const [show, setShow] = useState(false);
    const [show1, setShow1] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const handleClose1 = () => setShow1(false);
    const handleShow1 = () => setShow1(true);

    const [id, setId] = useState("");
    const [name, setName] = useState("");
    const [cif, setCif] = useState("");
    const [reg, setReg] = useState("");
    const [email, setEmail] = useState("");
    const [address, setAddress] = useState("");
    const [paymentDays, setPaymentDays] = useState();

    const [companies, setCompanies] = useState([]);

    async function refreshCompanies() {
        await fetch(host.companies, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }).then(response => response.json())
            .then(result => {
                console.log(result);
                if (result.status === 1) {
                    setCompanies(result.data);
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }

    async function addCompany() {
        const body = {name, cif, reg, email, address, paymentDays}
        await fetch(host.companies, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }).then(response => response.json())
            .then(result => {
                console.log(result);
                if (result.status === 1) {
                    alert("SUCCESS: Added to database!");
                }
            })
            .catch(error => {
                console.error('Error:', error);
                alert("ERROR: Something went wrong");
            });

        setShow(false);
        refreshCompanies();
    }

    async function deleteCompany(id) {
        await fetch(host.companies + id, {
            method: 'DELETE',
            headers: {
                'Accept': '*',
                'Content-Type': 'application/json'
            },
        })
            .then(response => response.json())
            .then(result => {
                console.log(result);
            });
        refreshCompanies();
    }

    function editCompany(company) {
        const {id, name, cif, reg, email, address, paymentDays} = company
        setId(id);
        setName(name);
        setCif(cif);
        setReg(reg);
        setEmail(email);
        setAddress(address);
        setPaymentDays(paymentDays);
        handleShow1();
    }

    async function editFinal() {
        const body = {id, name, cif, reg, email, address, paymentDays}
        await fetch(host.companies, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                'Accept': '*',
                'Content-Type': 'application/json'
            },

        })
            .then(response => response.json())
            .then(result => {
                console.log(result);
            });
        handleClose1();
        refreshCompanies();
    }


    return (
        <div>
            <div>
                <Card>
                    <Card.Header>Welcome back</Card.Header>
                </Card>
            </div>

            <div className="admin">
                <Button variant="outline-success" onClick={handleShow}>Add Company</Button>
            </div>

            <div className="admin">
                <Button variant="outline-secondary" onClick={refreshCompanies}>Refresh Companies</Button>
            </div>


            <div className="admin">
                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Cif</th>
                        <th>Reg</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>PaymentDays</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {companies.map((company, index) => {
                        const {id, name, cif, reg, email, address, paymentDays} = company
                        return (
                            <tr key={id}>
                                <td>{name}</td>
                                <td>{cif}</td>
                                <td>{reg}</td>
                                <td>{email}</td>
                                <td>{address}</td>
                                <td>{paymentDays}</td>
                                <td>
                                    <div>
                                        <Button style={{marginRight: "10px"}} onClick={() => deleteCompany(id)}
                                                variant="outline-danger">Delete</Button>
                                        <Button onClick={() => editCompany(company)}
                                                variant="outline-warning">Edit</Button>
                                    </div>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
            </div>


            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add a new Company</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" onChange={e => setName(e.currentTarget.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Cif</Form.Label>
                            <Form.Control type="text" onChange={e => setCif(e.currentTarget.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Reg</Form.Label>
                            <Form.Control type="text" onChange={e => setReg(e.currentTarget.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" onChange={e => setEmail(e.currentTarget.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Address</Form.Label>
                            <Form.Control type="text" onChange={e => setAddress(e.currentTarget.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>PaymentDays</Form.Label>
                            <Form.Control type="number" onChange={e => setPaymentDays(e.currentTarget.value)}/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="outline-success" onClick={addCompany}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>

            <Modal show={show1} onHide={handleClose1}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Company</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>Name</Form.Label>
                            <Form.Control type="text" onChange={e => setName(e.currentTarget.value)}
                                          value={name}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Cif</Form.Label>
                            <Form.Control type="text" onChange={e => setCif(e.currentTarget.value)} value={cif}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Reg</Form.Label>
                            <Form.Control type="text" onChange={e => setReg(e.currentTarget.value)} value={reg}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" onChange={e => setEmail(e.currentTarget.value)}
                                          value={email}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>Address</Form.Label>
                            <Form.Control type="text" onChange={e => setAddress(e.currentTarget.value)}
                                          value={address}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>PaymentDays</Form.Label>
                            <Form.Control type="number" onChange={e => setPaymentDays(e.currentTarget.value)}
                                          value={paymentDays}/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary" onClick={handleClose1}>
                        Close
                    </Button>
                    <Button variant="outline-success" onClick={editFinal}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>


        </div>


    )
}