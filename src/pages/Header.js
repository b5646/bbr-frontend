import React, {useState} from "react";
import {Navbar, Nav, Container} from 'react-bootstrap';


export default function Header() {

    const [isUserLoggedIn, setIsUserLoggedIn] = useState(checkUserStatus());

    function checkUserStatus() {
        if (localStorage.getItem("logged-user") === null) {
            return false;
        } else {
            return true;
        }
    }


    function logout() {
        localStorage.removeItem("logged-user");
    }

    return (
        <div>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                    <Navbar.Brand href="/">BBR</Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav"/>
                    <Navbar.Collapse id="responsive-navbar-nav">

                        {isUserLoggedIn ?
                            <Nav className="me-auto">
                                <Nav.Link href="/companies">Companies</Nav.Link>
                                <Nav.Link href="/invoices">Invoices</Nav.Link>
                                <Nav.Link href="/codes">Codes</Nav.Link>
                            </Nav>
                            :
                            <Nav className="me-auto"/>
                        }

                        {isUserLoggedIn ?
                            <Nav>
                                <Nav.Link href="/" onClick={logout}>Logout</Nav.Link>
                            </Nav>
                            :
                            <Nav>
                                <Nav.Link href="/login">Login</Nav.Link>
                            </Nav>
                        }


                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    );
}