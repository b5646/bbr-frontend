import React, {useState, useEffect} from 'react'
import {Form, Button} from 'react-bootstrap';
import {useNavigate} from 'react-router-dom'
import {host} from "../util/hosts";

export default function Login() {

    const [email, setEmail] = useState("");
    const [securityCode, setSecurityCode] = useState("");
    const [isFirstVisible, setIsFirstVisible] = useState(true);
    const navigate = useNavigate();


    function refreshPage() {
        window.location.reload(false);
    }

    async function sendSecurityCode() {
        const body = {toEmail: email}
        await fetch(host.send_security_code, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Accept': '*',
                'Content-Type': 'application/json'
            },
        })
            .then(response => response.json())
            .then(result => {
                    console.log(result);
                    if (result.status === 1) {
                        alert("Verify your inbox: " + email);
                        setIsFirstVisible(false);
                    }
                }
            ).catch(error => {
                console.error('Error:', error);
            });
    }

    async function verifySecurityCode() {
        const body = {message: securityCode}
        await fetch(host.verify_security_code, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Accept': '*',
                'Content-Type': 'application/json'
            },
        })
            .then(response => response.json())
            .then(result => {
                    console.log(result);
                    if (result.status === 1) {
                        alert("Correct code! " + securityCode);
                        setIsFirstVisible(true);
                        localStorage.setItem("logged-user", "logged");
                        navigate('/admin');
                    }
                }
            ).catch(error => {
                console.error('Error:', error);
            });
        refreshPage();
    }


    return (
        <div>

            {isFirstVisible ?
                <div className="login-card">
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Floating><strong>Login to your ADMIN account</strong></Form.Floating>
                        </Form.Group>

                        <Form.Group className="mb-3" controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email"
                                          onChange={e => setEmail(e.currentTarget.value)}/>
                        </Form.Group>


                        <Button variant="outline-secondary" onClick={sendSecurityCode}>
                            Send security code
                        </Button>
                    </Form>
                </div>
                :
                <div className="login-card">
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Floating><strong>Input security code</strong></Form.Floating>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Security code</Form.Label>
                            <Form.Control type="text" placeholder="Enter security code"
                                          onChange={e => setSecurityCode(e.currentTarget.value)}/>
                        </Form.Group>


                        <Button variant="outline-secondary" onClick={verifySecurityCode}>
                            Verify security code
                        </Button>
                    </Form>
                </div>
            }


        </div>
    );
}

