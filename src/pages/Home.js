import React from "react";
import {Button, Card} from 'react-bootstrap';

export default function Home() {

    return (
        <div className="home">
            <Card className="text-center">
                <Card.Header>Home Page</Card.Header>
                <Card.Body>
                    <Card.Title>Build Better Routes</Card.Title>
                    <Card.Text>
                        A better way of managing your business. Easy and secure.
                    </Card.Text>
                    <Button variant="outline-secondary" href="/login">Go to my account</Button>
                </Card.Body>
                <Card.Footer className="text-muted">Coded with 🤎 by Andrei Petrascu 😊</Card.Footer>
            </Card>
        </div>
    );
}