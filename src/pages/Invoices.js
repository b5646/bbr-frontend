import React, {useState} from "react";
import {Col, Row, Card, Button, Modal, Form, Table, ListGroup} from 'react-bootstrap'

import {host} from "../util/hosts";
import download from 'downloadjs';

export default function Invoices() {

    //const user = JSON.parse(localStorage.getItem('logged-user'));

    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => {
        refreshInvoices();
        refreshCompanies();
        setTva(19);
        setCompany({});
        setName("");
        setPrice("");
        setQuantity("");
        setItems([]);
        setIsEur("");
        setShow(true);
    };

    const [show1, setShow1] = useState(false);
    const handleClose1 = () => setShow1(false);
    const handleShow1 = async (invoice) => {
        refreshCompanies();

        setTva(invoice.tva);
        setIsEur(invoice.isEur);
        setName("");
        setPrice("");
        setQuantity("");

        companies.forEach(e => {
            if (e.id == invoice.clientId) {
                setCompany(e);
            }
        });

        setShow1(true);
    };

    let invoiceCompany = "";
    let invoiceItems = [];

    const [isEur, setIsEur] = useState(true);
    const setEurTrue = () => setIsEur(true);
    const setEurFalse = () => setIsEur(false);

    const [companies, setCompanies] = useState([]);
    const [invoices, setInvoices] = useState([]);

    const [invoiceId, setInvoiceId] = useState();
    const [tva, setTva] = useState();
    const [company, setCompany] = useState({
        id: "",
        name: "",
        address: "",
        email: "",
        cif: "",
        reg: "",
        paymentDays: ""
    });

    const [id, setId] = useState("");
    const [name, setName] = useState("");
    const [price, setPrice] = useState("");
    const [quantity, setQuantity] = useState("");

    const [items, setItems] = useState([]);
    const [editItems, setEditItems] = useState([]);

    async function refreshInvoices() {
        await fetch(host.invoices, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }).then(response => response.json())
            .then(result => {
                console.log(result);
                if (result.status === 1) {
                    setInvoices(result.data);
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
        refreshCompanies();
    }

    async function refreshCompanies() {
        await fetch(host.companies, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }).then(response => response.json())
            .then(result => {
                console.log(result);
                if (result.status === 1) {
                    setCompanies(result.data);
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }

    async function addInvoice() {
        const body = {tva, isEur, company, items}
        console.log(body);
        await fetch(host.invoices, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }).then(response => response.json())
            .then(result => {
                console.log(result);
                if (result.status === 1) {
                    alert("SUCCESS: Added to database!");
                } else {
                    alert("ERROR [addInvoice]");
                }
            })
            .catch(error => {
                console.error('Error:', error);
                alert("ERROR: Something went wrong");
            });

        setShow(false);
        refreshInvoices();
    }

    async function handleDeleteButton(id) {
        await fetch(host.storno + id, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }).then(response => response.json())
            .then(result => {
                console.log(result);
                refreshInvoices();
            })
            .catch(error => {
                console.error('Error:', error);
                alert("ERROR: Something went wrong");
            });
    }

    async function handleEditButton(invoice) {
        await findAllItemsByInvoiceId(invoice.id);
        setInvoiceId(invoice.id);
        setEditItems([]);
        handleShow1(invoice);
    }

    async function editInvoice() {
        const body = {id: invoiceId, tva: tva, isEur: isEur, company: company, items: editItems}

        await fetch(host.invoices, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }).then(response => response.json())
            .then(result => {
                console.log(result);
                if (result.status === 1) {
                    alert("SUCCESS: Updated!");
                } else {
                    alert("ERROR");
                }
            })
            .catch(error => {
                console.error('Error:', error);
                alert("ERROR: [editInvoice] Something went wrong");
            });

        setShow1(false);
        refreshInvoices();
    }

    async function handleDownloadButton(invoice) {
        console.log("handleDownloadButton");
        await findCompanyById(invoice.clientId);
        await findAllItemsByInvoiceId(invoice.id);

        await getPdf(invoice);
    }

    async function getPdf(invoice) {
        const {id, invoiceCode, creationDate, dueDate, euro, tva, priceWithoutTva, priceTva, priceTotal, clientId, isEur, isStorno, stornoCode} = invoice;
        const body = {
            invoiceCode,
            creationDate,
            dueDate,
            euro,
            tva,
            priceWithoutTva,
            priceTva,
            priceTotal,
            isEur,
            isStorno,
            company: invoiceCompany,
            items: invoiceItems,
            stornoCode
        };
        await fetch(host.get_pdf, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }).then(response => response.blob())
            .then(result => {
                console.log(result);
                download(result, "Factura " + invoiceCode.slice(3, 9));
                //download(result.body, "output.pdf", result.headers['application/pdf']);
            })


            .catch(error => {
                console.error('Error:', error);
                alert("ERROR: Something went wrong");
            });
    }

    async function findCompanyById(id) {
        await fetch(host.companies + id, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }).then(response => response.json())
            .then(result => {
                if (result.status === 1) {
                    invoiceCompany = result.data;
                } else {
                    alert("ERROR findCompanyById");
                }
            })
            .catch(error => {
                console.error('Error:', error);
                alert("ERROR: Something went wrong");
            });
    }

    async function findAllItemsByInvoiceId(id) {
        await fetch(host.items + id, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }).then(response => response.json())
            .then(result => {
                if (result.status === 1) {
                    setItems(result.data);
                    invoiceItems = result.data;
                } else {
                    alert("ERROR findAllItemsByInvoiceId");
                }
            })
            .catch(error => {
                console.error('Error:', error);
                alert("ERROR: Something went wrong findAllItemsByInvoiceId");
            });
    }

    function onChangeCompany(event) {
        console.log('selected company: ' + event.target.value);
        companies.forEach(e => {
            if (e.id == event.target.value) {
                setCompany(e);
            }
        });
    }

    function handleAddItemButton() {
        if (name == null || price == null || quantity == null) {
            alert('name=null or price=null or quantity=null');
        } else {
            let lastItem = {name: name, price: price, quantity: quantity};
            let data = [...items];
            data[data.length] = lastItem;
            setItems(data);

            setName("");
            setPrice("");
            setQuantity("");
        }
    }

    function handleEditItemButton(item) {
        setId(item.id);
        setName(item.name);
        setPrice(item.price);
        setQuantity(item.quantity);
    }

    function handleSaveEditItemButton() {
        if (name == null || price == null || quantity == null || name === "" || price === "" || quantity === "") {
            alert('name, price or quantity - dont have a value');
        } else {
            let lastItem = {id: id, name: name, price: price, quantity: quantity};
            let data = [...editItems];
            data[data.length] = lastItem;
            setEditItems(data);

            setId("");
            setName("");
            setPrice("");
            setQuantity("");
        }
    }

    return (
        <div>
            <div>
                <Card>
                    <Card.Header>Welcome back</Card.Header>
                </Card>
            </div>

            <div className="admin">
                <Button variant="outline-success" onClick={handleShow}>Add Invoice</Button>
            </div>

            <div className="admin">
                <Button variant="outline-secondary" onClick={refreshInvoices}>Refresh Invoices</Button>
            </div>


            <div className="admin">
                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th>invoiceCode</th>
                        <th>creationDate</th>
                        <th>dueDate</th>
                        <th>euro</th>
                        <th>tva</th>
                        <th>priceWithoutTva</th>
                        <th>priceTva</th>
                        <th>priceTotal</th>
                        <th>clientId</th>
                        <th>isEur</th>
                        <th>isStorno</th>
                        <th>stornoCode</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {invoices.map((invoice, index) => {
                        const {id, invoiceCode, creationDate, dueDate, euro, tva, priceWithoutTva, priceTva, priceTotal, clientId, isEur, isStorno, stornoCode} = invoice //destructuring
                        return (
                            <tr key={id}>
                                <td>{invoiceCode}</td>
                                <td>{creationDate}</td>
                                <td>{dueDate}</td>
                                <td>{euro}</td>
                                <td>{tva}</td>
                                <td>{priceWithoutTva}</td>
                                <td>{priceTva}</td>
                                <td>{priceTotal}</td>
                                <td>{clientId}</td>
                                <td>{isEur === true ? "true" : "false"}</td>
                                <td>{isStorno === true ? "true" : "false"}</td>
                                <td>{stornoCode}</td>
                                <td>
                                    <div>
                                        <Button style={{marginRight: "10px"}} onClick={() => handleDeleteButton(id)}
                                                variant="outline-danger">Storno</Button>
                                        <Button style={{marginRight: "10px"}} onClick={() => handleEditButton(invoice)}
                                                variant="outline-warning">Edit</Button>
                                        <Button onClick={() => handleDownloadButton(invoice)}
                                                variant="outline-info">Download</Button>
                                    </div>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
            </div>


            <iframe className="exchange"
                    src="https://www.cursbnr.ro/insert/cursvalutar.php?w=200&b=f7f7f7&bl=dcdcdc&ttc=0a6eab&tc=000000&diff=1&ron=1&cb=1">CursBNR
            </iframe>


            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add a new Invoice</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>Tva</Form.Label>
                            <Form.Control type="number" value={tva} onChange={e => setTva(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Company: {company.name}</Form.Label>
                            <Form.Select onChange={onChangeCompany}>
                                <option value="">Select an Option</option>
                                {
                                    companies.map((cmp, index) => {
                                        return (<option key={cmp.id} value={cmp.id}>{cmp.name}</option>)
                                    })
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label><strong>Items</strong></Form.Label>
                            {items.map((item, index) =>
                                <p key={index}>
                                    Name: {item.name} Price: {item.price} Quantity: {item.quantity}
                                </p>
                            )}
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label><strong>Add</strong></Form.Label>
                            <div>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm="2">Name</Form.Label>
                                    <Col sm="10">
                                        <Form.Control type="text" value={name}
                                                      onChange={e => setName(e.target.value)}/>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm="2">Price</Form.Label>
                                    <Col sm="10">
                                        <Form.Control type="number" value={price}
                                                      onChange={e => setPrice(e.target.value)}/>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm="2">Quantity</Form.Label>
                                    <Col sm="10">
                                        <Form.Control type="number" value={quantity}
                                                      onChange={e => setQuantity(e.target.value)}/>
                                    </Col>
                                </Form.Group>
                                <Button onClick={handleAddItemButton}
                                        variant="outline-secondary">Add Item</Button>
                            </div>
                        </Form.Group>

                        <Form.Group>
                            <div key="inline-radio" className="mb-3">
                                <Form.Check
                                    inline
                                    label="eur"
                                    name="group1"
                                    type="radio"
                                    id="inline-radio-1"
                                    onChange={setEurTrue}
                                />
                                <Form.Check
                                    inline
                                    label="ron"
                                    name="group1"
                                    type="radio"
                                    id="inline-radio-2"
                                    onChange={setEurFalse}
                                />
                            </div>
                        </Form.Group>

                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="outline-success" onClick={addInvoice}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>


            <Modal show={show1} onHide={handleClose1}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Invoice</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>Tva: {tva}</Form.Label>
                            <Form.Control type="number" value={tva} onChange={e => setTva(e.target.value)}/>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label>Company: {company.name}</Form.Label>
                            <Form.Select onChange={onChangeCompany} value={company} placeholder={company}>
                                <option value="">Select an Option</option>
                                {
                                    companies.map((cmp, index) => {
                                        return (<option key={cmp.id} value={cmp.id}>{cmp.name}</option>)
                                    })
                                }
                            </Form.Select>
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label><strong>Items</strong></Form.Label>
                            {items.map((item, index) =>
                                <p key={index}>
                                    Name: {item.name} Price: {item.price} Quantity: {item.quantity}
                                    <Button onClick={() => handleEditItemButton(item)}
                                            variant="outline-secondary">Edit</Button>
                                </p>
                            )}
                        </Form.Group>

                        <Form.Group className="mb-3">
                            <Form.Label><strong>Edit item</strong></Form.Label>
                            <div>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm="2">Name</Form.Label>
                                    <Col sm="10">
                                        <Form.Control type="text" value={name}
                                                      onChange={e => setName(e.target.value)}/>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm="2">Price</Form.Label>
                                    <Col sm="10">
                                        <Form.Control type="number" value={price}
                                                      onChange={e => setPrice(e.target.value)}/>
                                    </Col>
                                </Form.Group>

                                <Form.Group as={Row} className="mb-3">
                                    <Form.Label column sm="2">Quantity</Form.Label>
                                    <Col sm="10">
                                        <Form.Control type="number" value={quantity}
                                                      onChange={e => setQuantity(e.target.value)}/>
                                    </Col>
                                </Form.Group>

                                <Button onClick={handleSaveEditItemButton}
                                        variant="outline-secondary">Edit Item</Button>
                            </div>
                        </Form.Group>

                        <Form.Group>
                            <Form.Label><strong>IsEur: </strong> {isEur === true ? "true" : "false"} </Form.Label>
                            <div key="inline-radio" className="mb-3">
                                <Form.Check
                                    inline
                                    label="eur"
                                    name="group1"
                                    type="radio"
                                    id="inline-radio-1"
                                    onChange={setEurTrue}
                                />
                                <Form.Check
                                    inline
                                    label="ron"
                                    name="group1"
                                    type="radio"
                                    id="inline-radio-2"
                                    onChange={setEurFalse}
                                />
                            </div>
                        </Form.Group>

                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary" onClick={handleClose1}>
                        Close
                    </Button>
                    <Button variant="outline-success" onClick={editInvoice}>
                        Save Changes
                    </Button>
                </Modal.Footer>
            </Modal>


        </div>


    )
}