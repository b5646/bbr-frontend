import React from "react";
import {Button, Card} from 'react-bootstrap';


export default function Admin() {

    return (
        <Card>
            <Card.Header>Admin Page</Card.Header>
            <Card.Body>
                <blockquote className="blockquote mb-0">
                    <p>
                        {' '}
                        The Admin page gives you access to the Analytics administrative features. At the bottom-left of
                        the page, hover over the gear icon and click Admin to open{' '}
                    </p>
                    <footer className="blockquote-footer">
                        Someone famous in <cite title="Source Title">Source Title</cite>
                    </footer>
                </blockquote>
            </Card.Body>
        </Card>
    );
}