import React, {useState} from "react";
import {Card, Button, Modal, Form, Table} from 'react-bootstrap'

import {host} from "../util/hosts";


export default function Codes() {


    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [id, setId] = useState();
    const [ronCode, setRonCode] = useState();
    const [eurCode, setEurCode] = useState();
    const [tvaZeroCode, setTvaZeroCode] = useState();

    const [codes, setCodes] = useState([]);


    async function refreshCodes() {
        await fetch(host.codes, {
            method: 'GET',
            headers: {
                "Content-Type": "application/json",
                "Accept": "application/json"
            }
        }).then(response => response.json())
            .then(result => {
                console.log(result);
                if (result.status === 1) {
                    setCodes(result.data);
                    /*setId(result.data[0].id);
                    setRonCode(result.data[0].ronCode);
                    setEurCode(result.data[0].eurCode);
                    setTvaZeroCode(result.data[0].tvaZeroCode);*/
                }
            })
            .catch(error => {
                console.error('Error:', error);
            });
    }


    function editButton(code) {
        const {id, eurCode, ronCode, tvaZeroCode} = code;
        setId(id);
        setEurCode(eurCode);
        setRonCode(ronCode);
        setTvaZeroCode(tvaZeroCode);
        handleShow();
    }

    async function editFinal() {
        const body = {id, ronCode, eurCode, tvaZeroCode}
        await fetch(host.codes, {
            method: 'PUT',
            body: JSON.stringify(body),
            headers: {
                'Accept': '*',
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .then(result => {
                console.log(result);
            });
        handleClose();
        refreshCodes();
    }


    return (
        <div>
            <div>
                <Card>
                    <Card.Header>Welcome back</Card.Header>
                </Card>
            </div>


            <div className="admin">
                <Button variant="outline-secondary" onClick={refreshCodes}>Refresh Codes</Button>
            </div>


            <div className="admin">
                <Table striped bordered hover size="sm">
                    <thead>
                    <tr>
                        <th>ronCode</th>
                        <th>eurCode</th>
                        <th>tvaZeroCode</th>

                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    {codes.map((code, index) => {
                        const {id, ronCode, eurCode, tvaZeroCode} = code
                        return (
                            <tr key={id}>
                                <td>{ronCode}</td>
                                <td>{eurCode}</td>
                                <td>{tvaZeroCode}</td>

                                <td>
                                    <div>

                                        <Button onClick={() => editButton(code)}
                                                variant="outline-warning">Edit</Button>
                                    </div>
                                </td>
                            </tr>
                        )
                    })}
                    </tbody>
                </Table>
            </div>


            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Codes</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form>
                        <Form.Group className="mb-3">
                            <Form.Label>RonCode</Form.Label>
                            <Form.Control type="number" value={ronCode} onChange={e => setRonCode(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>EurCode</Form.Label>
                            <Form.Control type="number" value={eurCode} onChange={e => setEurCode(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="mb-3">
                            <Form.Label>TvaZeroCode</Form.Label>
                            <Form.Control type="number" value={tvaZeroCode}
                                          onChange={e => setTvaZeroCode(e.target.value)}/>
                        </Form.Group>
                    </Form>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="outline-secondary" onClick={handleClose}>Close</Button>
                    <Button variant="outline-success" onClick={editFinal}>Save Changes</Button>
                </Modal.Footer>
            </Modal>


        </div>


    )
}