import './App.css';
import React from "react";
import {BrowserRouter, Routes, Route} from "react-router-dom";

import Header from "./pages/Header";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Admin from "./pages/Admin";
import Companies from "./pages/Companies";
import Invoices from "./pages/Invoices";
import Codes from "./pages/Codes";

function App() {
    return (
        <BrowserRouter>
            <Header/>
            <Routes>
                <Route path="/login" element={<Login/>}/>
                <Route path="/admin" element={<Admin/>}/>
                <Route path="/companies" element={<Companies/>}/>
                <Route path="/invoices" element={<Invoices/>}/>
                <Route path="/codes" element={<Codes/>}/>


                <Route path="/" element={<Home/>}/>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
