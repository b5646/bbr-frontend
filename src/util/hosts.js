
export const api =
    {
        //api_gateway: 'http://localhost:7000'
        api_gateway: 'https://bbr-api-gateway.azurewebsites.net'
    };

export const host = {
    send_security_code: api.api_gateway + '/send-security-code',
    verify_security_code: api.api_gateway + '/verify-security-code',
    companies: api.api_gateway + '/companies/',
    invoices: api.api_gateway + '/invoices/',
    items: api.api_gateway + '/items/',
    storno: api.api_gateway + '/storno/',
    get_pdf: api.api_gateway + '/get-pdf',
    codes: api.api_gateway + '/codes'

}